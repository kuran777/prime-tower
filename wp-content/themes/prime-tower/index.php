<?php
/*
Template Name: Home
Template Post Type: page
 */

$index_page_title = get_the_title(get_option('page_for_posts', true));
get_header();
?>
    <main class="home-page">
        <div class="full-thumbnail"
             style="background: url(<?=the_post_thumbnail_url('single-post-thumbnail');?>);">
			<div class="container">
			<div class="home-maine-text">
				Über den Dächern von Zürich<br>
				<span style="font-size: 90px;font-weight:500;line-height:100px;font-family: Roboto;">Your Prime <p> Site in Zurich</span>
			</div>
			</div>
        </div>
        <section class="section-business">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-xs-12 col-sm-11 col-lg-10 text-center business-text" data-aos="zoom-in-up">
Wer sich hohe Ziele steckt, findet im Prime Tower Areal den perfekten Geschäftsstandort.
Das architektonisch herausragende Bauwerk von Gigon/Guyer ist das Wahrzeichen des
wachsenden Businessbezirks von Zürich-West und wie gemacht für Unternehmen mit
allerhöchsten Ansprüchen.
Das Prime Tower Areal besteht aus fünf Gebäuden mit Büro-, Retail-, Gewerbe-,
Gastronomie- sowie Kulturräumen und profitiert vom pulsierenden Business Flair des
Quartiers – und vom optimalen Mix zahlreicher hervorragend vernetzter Unternehmen. So
beleben fünf etablierte Gastronomiekonzepte, ein Fitnesscenter und eine Kindertagesstätte
das Areal ebenso wie zwei gut frequentierte Detailhändler, eine Bank, diverse
Kulturschaffende und weitere Dienstleister. Und der 126 Meter hohe Prime Tower überragt
mit seinen 36 Etagen nicht nur die Skyline der City – er bietet auch attraktive Perspektiven in
eine erfolgreiche Zukunft.<br><br>
Herzlich willkommen im Prime Tower Areal.
                    </div>
                </div>
    <!--            <div class="row">-->
    <!--                <div class="col-12">-->
    <!--                    --><?php
    //                    if (have_posts()):
    //
    //                        while (have_posts()) : the_post();
    //                            get_template_part('parts/posts/content', 'index');
    //                        endwhile;
    //
    //                    else:
    //                        get_template_part('parts/posts', 'none');
    //                    endif;
    //                    ?>
    <!--                </div>-->
    <!--                <div class="">-->
    <!--                    --><?php //get_sidebar(); ?>
    <!--                </div>-->
    <!--            </div>-->
            </div>
            <div class="container-fluid px-0 home-pages">
                <div class="row page-block no-gutters mx-0" data-aos="fade-right">
                    <div class="col-12 col-md-5 col-lg-4 page-data">
                        <div class="block-title">Business
						</div>
						<font style="font-weight: lighter" size="3" color="#FFFFFF">Highlights auf 36 Etagen</font>
						<font style="font-weight: lighter" size="3" color="#FFFFFF">Der Prime Tower kombiniert herausragende</font>
						<font style="font-weight: lighter" size="3" color="#FFFFFF">Infrastruktur mit perfekten Arbeitsbedingungen.</font>
                        <div  class="arrow" data-aos="fade-right" data-aos-delay="100">
							<a target="_blank" href="/business/"><img src="<?=get_template_directory_uri();?>/assets/images/arrow-right.svg" alt="" ></a>
                        </div>
                    </div>
                    <div class="col-12 col-md-7 col-lg-8 page-image">
                        <img src="/wp-content/uploads/2019/03/Bildschirmfoto-2019-03-12-um-10.55.49.png" alt="" >
                    </div>
                </div>
                <div class="row page-block no-gutters mx-0" data-aos="fade-left">
                    <div class="col-12 col-md-7 col-lg-8 order-md-first order-last page-image">
                        <img src="/wp-content/uploads/2019/03/Bildschirmfoto-2019-03-12-um-10.53.04.png" alt="">
                        <div class="page-logo" data-aos="fade-right" data-aos-offset="300" data-aos-easing="ease-in-sine">
                            <img src="<?=get_template_directory_uri();?>/assets/images/gastronomie-logo.png" alt="">
                        </div>
                    </div>
                    <div class="col-12 col-md-5 col-lg-4 order-md-last order-first page-data">
                        <div class="block-title">Gastronomie</div>
						<font style="font-weight: lighter" size="3" color="#FFFFFF">Wenn das Auge mitisst</font>
						<font style="font-weight: lighter" size="3" color="#FFFFFF">Das stilvolle Restaurant Clouds</font>
						<font style="font-weight: lighter" size="3" color="#FFFFFF">im Prime Tower serviert hohe Kochkunst</font>
						<font style="font-weight: lighter" size="3" color="#FFFFFF">samt spektakulären Aussichten.</font>
                        <div class="arrow" data-aos="fade-right" data-aos-delay="100">
							<a target="_blank" href="/gastronomie/"><img src="<?=get_template_directory_uri();?>/assets/images/arrow-right.svg" alt=""></a>
                        </div>
                    </div>
                </div>
                <div class="row page-block no-gutters mx-0" data-aos="fade-right">
                    <div class="col-12 col-md-5 col-lg-4 page-data text-center">
                        <div class="block-title">Areal</div>
						<font style="font-weight: lighter" size="3" color="#FFFFFF">Zukunft inklusive</font>
						<font style="font-weight: lighter" size="3" color="#FFFFFF">Das repräsentative Prime Tower Areal</font>
						<font style="font-weight: lighter" size="3" color="#FFFFFF">bietet im dynamischen Zürich-West ideale</font>
						<font style="font-weight: lighter" size="3" color="#FFFFFF">Standortbedingungen für anspruchsvolle Unternehmen.</font>
                        <div class="arrow" data-aos="fade-right" data-aos-delay="100">
							<a target="_blank" href="/areal/"><img src="<?=get_template_directory_uri();?>/assets/images/arrow-right.svg" alt=""></a>
                        </div>
                    </div>
                    <div class="col-12 col-md-7 col-lg-8 page-image">
                        <img src="/wp-content/uploads/2019/03/Bildschirmfoto-2019-03-12-um-10.53.45.png" alt="">
                    </div>
                </div>
            </div>
        </section>
        <section class="metric-block">
            <div class="container-fluid">
                <div class="row number-line justify-content-center align-items-center no no-gutters">
                    <div class="col-12 col-md-3 d-flex justify-content-center number-item" data-aos="fade-left" data-aos-anchor-placement="center-bottom" data-aos-delay="50">
                        <div class="number-box">228 000 m<sup>3</sup></div>
                        <div class="after-text">Volumen</div>
                    </div>
                    <div class="col-12 col-md-3 d-flex justify-content-center number-item" data-aos="fade-left" data-aos-anchor-placement="center-bottom" data-aos-delay="100">
                        <div class="number-box">40 000 m<sup>2</sup></div>
                        <div class="after-text">Nutzfläche</div>
                    </div>
                    <div class="col-12 col-md-2 d-flex justify-content-center number-item" data-aos="fade-left" data-aos-anchor-placement="center-bottom" data-aos-delay="150">
                        <div class="number-box">2 000</div>
                        <div class="after-text">Arbeitsplätze</div>
                    </div>
                    <div class="col-12 col-md-2 d-flex justify-content-center number-item" data-aos="fade-left" data-aos-anchor-placement="center-bottom" data-aos-delay="200">
                        <div class="number-box">36</div>
                        <div class="after-text">Stockwerke</div>
                    </div>
                    <div class="col-12 col-md-2 d-flex justify-content-center number-item" data-aos="fade-left" data-aos-anchor-placement="center-bottom" data-aos-delay="250">
                        <div class="number-box">126 m</div>
                        <div class="after-text">Höhe</div>
                    </div>
                </div>
            </div>
        </section>
        <section class="section-map">
            <div class="container-fluid px-0 home-map">
                <div class="row no-gutters mx-0">
                    <div class="col-12 col-md-12 col-lg-12 col-xl-8 map-image">
                        <img src="/wp-content/uploads/2019/03/Primetower_Lageplan.png" alt="">
                    </div>
                    <div class="col-12 col-md-12 col-lg-12 col-xl-4 map-data">
                        <div class="title" data-aos="fade-up" data-aos-anchor-placement="center-bottom" data-aos-delay="100">Perfekte Lage</div>
                        <p data-aos="fade-up" data-aos-anchor-placement="center-bottom" data-aos-delay="200">Die Verkehrsanbindung des Prime Tower ist erstklassig: Der Bahnhof Hardbrücke ist
unmittelbar daneben, Tram- und Busstationen nur in wenigen Schritten zu erreichen. Und der nächste Autobahnzubringer ist quasi gleich um die Ecke. Die zweigeschossige Tiefgarage und benachbarte Parkhäuser bieten genügend Parkplätze.</p>
                        <div class="map-table" data-aos="fade-up" data-aos-anchor-placement="center-bottom" data-aos-delay="300">
                            <div class="table-head">
                                <div></div>
                                <div class="car">
                                    <img src="<?=get_template_directory_uri();?>/assets/images/car.svg" alt="car">
                                </div>
                                <div></div>
                                <div class="bus">
                                    <img src="<?=get_template_directory_uri();?>/assets/images/bus.svg" alt="bus">
                                </div>
                                <div ></div>
                            </div>
                            <div class="table-row">
                                <div class="title">Hauptbahnhof Zürich</div>
                                <div class="time">10 min</div>
                                <div></div>
                                <div class="time">3 min</div>
                                <div></div>
                            </div>
                            <div class="table-row">
                                <div class="title">Flughafen Zürich</div>
                                <div class="time">17 min</div>
                                <div></div>
                                <div class="time">11 min</div>
                                <div></div>
                            </div>
                            <div class="table-row">
                                <div class="title">Zürichsee</div>
                                <div class="time">22 min</div>
                                <div></div>
                                <div class="time">10 min</div>
                                <div></div>
                            </div>
                            <div class="table-row">
                                <div class="title">Zürich City</div>
                                <div class="time">15 min</div>
                                <div></div>
                                <div class="time">5 min</div>
                                <div></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="aktuell">
            <div class="container">
                <div class="row justify-content-md-center">
                    <div class="col-md-12 aktuell-main-title">Aktuell</div>
                    <div class="col-md-6 col-lg-4 aktuell-mobile-content">
                        <img class="" src="/wp-content/uploads/2019/04/img_014.png">
                        <div class="home-post-title">
                            Ihr neuer Geschäftssitz?
                        </div>
                        <div class="home-post-date">
                            20. Februar 2019
                        </div>
                        <div class="home-post-text">
                            Im Prime Tower sind hochkarätige Büroflächen zu vermieten.
                        </div>
                        <div class="mobile-aktuell-position"><a class="eModal-3 post-home-btn" href>Freie Flächen</a></div>
                    </div>
                    <div class="col-md-6 col-lg-4  aktuell-mobile-content">
                        <img class="" src="/wp-content/uploads/2019/04/img_012.png">
                        <div class="home-post-title">
                            Flexx Ballet
                        </div>
                        <div class="home-post-date">
                            2. März 2019
                        </div>
                        <div class="home-post-text">
                            3D Dance Show Life Stories: ein faszinierender Mix aus klassischem Ballet, Breakdance, Tango und mehr.
                        </div>
                        <div class="mobile-aktuell-position"><a target="_blank" class="post-home-btn" href="https://bymaag.ch/">MAAG Halle</a></div>
                    </div>
                    <div class="col-md-6 col-lg-4 aktuell-mobile-content">
                        <img class="" src="/wp-content/uploads/2019/04/img_013.png">
                        <div class="home-post-title">
                            #primetower
                        </div>
                        <div class="home-post-date">
                            10. März 2019
                        </div>
                        <div class="home-post-text">
                            Seit seiner Eröffnung im Dezember 2011 ist der Prime Tower ein viel besuchtes Wahrzeichen Zürichs.
                        </div>
                        <div class="mobile-aktuell-position"><a target="_blank" class="post-home-btn" href="https://www.instagram.com/primetower/">Impressionen</a></div>
                    </div>
                </div>
            </div>
        </section>
    </main>
<?php get_footer(); ?>