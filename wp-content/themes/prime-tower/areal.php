<?php
/*
Template Name: Areal
Template Post Type: page
 */
get_header(); ?>

    <main class="areal-page">
        <div class="full-thumbnail"
             style="background: url(<?=the_post_thumbnail_url('single-post-thumbnail');?>);">
        </div>
        <section class="vier-gebäude">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-9">
                        <div class="vier-gebäude-sub-title">Alles zum Areal</div>
                        <div class="vier-gebäude-title">Hier finden Sie Facts & Figures</div>
                        <div class="vier-gebäude-text">
                           Der Prime Tower ist ein architektonisch herausragendes Werk von Gigon/Guyer und prägt
                            seit seinem Bau 2011 den Blick über die Dächer von Zürich. Eigentümerin des Prime Tower
                            Areal ist Swiss Prime Site.
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="prime-tower">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-lg-7 col-xl-7">
                        <div class="img-container">
                            <img class="building-map" src="<?=get_template_directory_uri();?>/assets/images/areal/primeTower.png" usemap="#image-map">
                            <map name="image-map">
                                <area onmouseleave="imgMapLeave()" onmouseenter="imgMapEnter('primeTower')" onclick="imgMapClick('primeTower')" alt="" nohref coords="579,179,717,125,840,35,1091,4,1207,121,1210,421,1182,434,1182,1236,1044,1273,967,1320,939,1314,934,1282,777,1238,642,1260,631,1322,607,1290,612,1191,586,1165,589,1066,578,760,575,555,579,351,579,271" shape="poly">
                                <area onmouseleave="imgMapLeave()" onmouseenter="imgMapEnter('cubus')" onclick="imgMapClick('cubus')" alt="" nohref coords="8,1042,439,971,495,1114,499,1322,411,1422,36,1318,12,1262" shape="poly">
                                <area onmouseleave="imgMapLeave()" onmouseenter="imgMapEnter('diagonal')" onclick="imgMapClick('diagonal')" alt="" nohref coords="639,1449,643,1274,775,1238,926,1282,934,1322,1122,1361,1162,1342,1214,1349,1230,1549,1182,1609,659,1477" shape="poly">
                                <area onmouseleave="imgMapLeave()" onmouseenter="imgMapEnter('platform')" onclick="imgMapClick('platform')" alt="" nohref coords="1329,1665,1329,1449,1433,1253,1401,1153,1409,990,1601,942,1705,1094,1709,1189,1733,1221,1813,1201,2108,1589,2096,1756,2084,1820,2048,1864,1653,1756" shape="poly">
                                <area onmouseleave="imgMapLeave()" onmouseenter="imgMapEnter('eventblock')" onclick="imgMapClick('eventblock')" alt="" nohref coords="299,2283,124,1708,124,1509,264,1477,299,1545,299,1581,555,1585,603,1661,739,1629,739,1557,854,1565,1166,1669,1174,1736,1254,1832,1306,1832,1341,1876,1345,2036" shape="poly">
                            </map>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-5 col-xl-5">
                        <div class="building-content primeTower">
                            <div class="row">
                                <div class="colo-md-12 prime-tower-title">PRIME TOWER</div>
                            </div>
                            <div class="row prime-tower-content">
                                <div class="col-md-5 text-left prime-tower-left-content">Adresse</div>
                                <div class="col-md-7 text-right">Hardstrasse 201, 8005 Zürich</div>
                            </div>
                            <div class="row prime-tower-content">
                                <div class="col-md-7 text-left prime-tower-left-content">Anzahl Geschosse</div>
                                <div class="col-md-5 text-right">36</div>
                            </div>
                            <div class="row prime-tower-content">
                                <div class="col-md-7 text-left prime-tower-left-content">Höhe</div>
                                <div class="col-md-5 text-right">126 m</div>
                            </div>
                            <div class="row prime-tower-content">
                                <div class="col-md-7 text-left prime-tower-left-content">Total Arbeitsplätze</div>
                                <div class="col-md-5 text-right">ca. 2 000</div>
                            </div>
                            <div class="row prime-tower-content">
                                <div class="col-md-7 text-left prime-tower-left-content">Total Mietfläche</div>
                                <div class="col-md-5 text-right">ca. 40 000 m2</div>
                            </div>
                            <div class="row prime-tower-content">
                                <div class="col-md-7 text-left prime-tower-left-content">Volumen</div>
                                <div class="col-md-5 text-right">ca. 228 000 m3</div>
                            </div>
                            <div class="row prime-tower-content">
                                <div class="col-md-7 text-left prime-tower-left-content">Parkplätze (inkl. Platform)</div>
                                <div class="col-md-5 text-right">ca. 250</div>
                            </div>
                            <div class="row prime-tower-content">
                                <div class="col-md-3 text-left prime-tower-left-content">Nutzung</div>
                                <div class="col-md-9 text-left prime-tower-content-lust">
                                    Der Prime Tower ist ein Geschäftshochhaus mit
                                    Dienstleistungsflächen, Bar im Erdgeschoss und
                                    öffentlichem Restaurant mit Bar und Lounge im
                                    Dachgeschoss. Die Mieterschaft kommt hauptsächlich aus
                                    dem gehobenen Dienstleistungssektor.
                                </div>
                            </div>
                        </div>
                        <div class="building-content cubus d-none">
                            <div class="row">
                                <div class="colo-md-12 prime-tower-title">CUBUS</div>
                            </div>
                            <div class="row prime-tower-content">
                                <div class="col-md-5 text-left prime-tower-left-content">Address</div>
                                <div class="col-md-7 text-right">Hardstrasse 221, 8005 Zurich</div>
                            </div>
                            <div class="row prime-tower-content">
                                <div class="col-md-7 text-left prime-tower-left-content">Number of floors</div>
                                <div class="col-md-5 text-right">7</div>
                            </div>
                            <div class="row prime-tower-content">
                                <div class="col-md-7 text-left prime-tower-left-content">Total rental space</div>
                                <div class="col-md-5 text-right">5,300 m2</div>
                            </div>
                            <div class="row prime-tower-content">
                                <div class="col-md-7 text-left prime-tower-left-content">Max. rental space per floor</div>
                                <div class="col-md-5 text-right">810 m2</div>
                            </div>
                            <div class="row prime-tower-content">
                                <div class="col-md-7 text-left prime-tower-left-content">Volume</div>
                                <div class="col-md-5 text-right">35,000 m3</div>
                            </div>
                            <div class="row prime-tower-content">
                                <div class="col-md-3 text-left prime-tower-left-content">Use</div>
                                <div class="col-md-9 text-left prime-tower-content-lust">
                                    The annex building offers space for small
                                    offices and archives. Tenants include a training centre,
                                    a creche as well as a Coop Pronto.
                                </div>
                            </div>
                        </div>
                        <div class="building-content diagonal d-none">
                            <div class="row">
                                <div class="colo-md-12 prime-tower-title">DIAGONAL</div>
                            </div>
                            <div class="row prime-tower-content">
                                <div class="col-md-5 text-left prime-tower-left-content">Address</div>
                                <div class="col-md-7 text-right">Maagplatz 3, 8005 Zurich</div>
                            </div>
                            <div class="row prime-tower-content">
                                <div class="col-md-7 text-left prime-tower-left-content">Number of floors</div>
                                <div class="col-md-5 text-right">5</div>
                            </div>
                            <div class="row prime-tower-content">
                                <div class="col-md-7 text-left prime-tower-left-content">Total rental space</div>
                                <div class="col-md-5 text-right">2,100 m2</div>
                            </div>
                            <div class="row prime-tower-content">
                                <div class="col-md-7 text-left prime-tower-left-content">Max. rental space per floor</div>
                                <div class="col-md-5 text-right">550 m2</div>
                            </div>
                            <div class="row prime-tower-content">
                                <div class="col-md-7 text-left prime-tower-left-content">Volume</div>
                                <div class="col-md-5 text-right">18,000 m3</div>
                            </div>
                            <div class="row prime-tower-content">
                                <div class="col-md-3 text-left prime-tower-left-content">Use</div>
                                <div class="col-md-9 text-left prime-tower-content-lust">
                                    The annex building has been restored and houses a
                                    restaurant on the ground and
                                    first floor while two international galleries extend
                                    between the second and fourth floors.
                                </div>
                            </div>
                        </div>
                        <div class="building-content platform d-none">
                            <div class="row">
                                <div class="colo-md-12 prime-tower-title">PLATFORM</div>
                            </div>
                            <div class="row prime-tower-content">
                                <div class="col-md-5 text-left prime-tower-left-content">Address</div>
                                <div class="col-md-7 text-right">Maagplatz 1, 8005 Zurich</div>
                            </div>
                            <div class="row prime-tower-content">
                                <div class="col-md-7 text-left prime-tower-left-content">Number of floors</div>
                                <div class="col-md-5 text-right">7</div>
                            </div>
                            <div class="row prime-tower-content">
                                <div class="col-md-7 text-left prime-tower-left-content">Number of workspaces</div>
                                <div class="col-md-5 text-right">ca. 1,000</div>
                            </div>
                            <div class="row prime-tower-content">
                                <div class="col-md-7 text-left prime-tower-left-content">Total rental space</div>
                                <div class="col-md-5 text-right">ca. 20,900 m2</div>
                            </div>
                            <div class="row prime-tower-content">
                                <div class="col-md-7 text-left prime-tower-left-content">Volume</div>
                                <div class="col-md-5 text-right">ca. 109,000 m3</div>
                            </div>
                            <div class="row prime-tower-content">
                                <div class="col-md-3 text-left prime-tower-left-content">Use</div>
                                <div class="col-md-9 text-left prime-tower-content-lust">
                                    The office building with its generous foyer is home to
                                    the Zurich office of the accountancy and consulting firm EY.
                                    A Coop branch completes the range of services offered.
                                </div>
                            </div>
                        </div>
                        <div class="building-content eventblock d-none">
                            <div class="row">
                                <div class="colo-md-12 prime-tower-title">EVENTBLOCK-MAAG</div>
                            </div>
                            <div class="row prime-tower-content">
                                <div class="col-md-5 text-left prime-tower-left-content">Address</div>
                                <div class="col-md-7 text-right">Zahnradstr. 22/24, 8005 Zurich</div>
                            </div>
                            <div class="row prime-tower-content">
                                <div class="col-md-7 text-left prime-tower-left-content">Number of floors</div>
                                <div class="col-md-5 text-right">5</div>
                            </div>
                            <div class="row prime-tower-content">
                                <div class="col-md-7 text-left prime-tower-left-content">Total rental space</div>
                                <div class="col-md-5 text-right">ca. 7,200 m2</div>
                            </div>
                            <div class="row prime-tower-content">
                                <div class="col-md-3 text-left prime-tower-left-content">Use</div>
                                <div class="col-md-9 text-left prime-tower-content-lust">
                                    The industrial complex is made up of various office
                                    floors and halls. The halls regularly play host to concerts,
                                    musicals and theatre productions.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="impressionen">
            <div class="container">
                <div class="row">
                    <?=do_shortcode('[instagram-feed]')?>
                </div>

<!--                <div class="row">-->
<!--                    <div class="col-md-4">-->
<!--                        <img class="impressionen-img" src="/wp-content/uploads/2019/04/Group622.png" alt="">-->
<!--                        <div class="impressionen-date">20. Februar 2019</div>-->
<!--                        <div class="impressionen-text">-->
<!--                            MyZurich #zurich #zürichcity #züricharchitecture-->
<!--                            #architectureswitzerland #swissarchitecture-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="col-md-4">-->
<!--                        <img class="impressionen-img" src="/wp-content/uploads/2019/04/Group623.png" alt="">-->
<!--                        <div class="impressionen-date">23. Februar 2019</div>-->
<!--                        <div class="impressionen-text">-->
<!--                            Züri West is best #inspiration #gifts #conceptstore-->
<!--                            #shopping #jewelry #buylocal #beauties #bijoux #accessories-->
<!--                            #makeotherpeoplehappy #letthemsmile #primetower-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="col-md-4">-->
<!--                        <img class="impressionen-img" src="/wp-content/uploads/2019/04/Group624.png" alt="">-->
<!--                        <div class="impressionen-date">1. März 2019</div>-->
<!--                        <div class="impressionen-text">-->
<!--                            Best Food in Zurich #clouds #zurichfood-->
<!--                            #ilovezurich #zurichlife #zurichstyle #avantapres-->
<!--                            #zurichmoodboard-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->

                <div class="impressionen-main-title">Impressionen</div>
                <div class="row impressionen-img-cont">

                    <div class="col-md-4">
                        <a href="/wp-content/uploads/2019/04/Prime_Tower_Impressionen_1.jpg" rel="prettyPhoto">
                            <img src="/wp-content/uploads/2019/04/Prime_Tower_Impressionen_1.jpg" alt="">
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a href="/wp-content/uploads/2019/04/Prime_Tower_Impressionen_2.jpg" rel="prettyPhoto">
                            <img src="/wp-content/uploads/2019/04/Prime_Tower_Impressionen_2.jpg" alt="">
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a href="/wp-content/uploads/2019/04/Prime_Tower_Impressionen_3.jpg" rel="prettyPhoto">
                            <img src="/wp-content/uploads/2019/04/Prime_Tower_Impressionen_3.jpg" alt="">
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a href="/wp-content/uploads/2019/04/Prime_Tower_Impressionen_4.jpg" rel="prettyPhoto">
                            <img src="/wp-content/uploads/2019/04/Prime_Tower_Impressionen_4.jpg" alt="">
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a href="/wp-content/uploads/2019/04/Prime_Tower_Impressionen_5.jpg" rel="prettyPhoto">
                            <img src="/wp-content/uploads/2019/04/Prime_Tower_Impressionen_5.jpg" alt="">
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a href="/wp-content/uploads/2019/04/Prime_Tower_Impressionen_6.jpg" rel="prettyPhoto">
                            <img src="/wp-content/uploads/2019/04/Prime_Tower_Impressionen_6.jpg" alt="">
                        </a>
                    </div>
                </div>
            </div>
        </section>
        <section class="zürich-west">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-12 text-center zürich-west-title">
                        Zürich-West
                    </div>
                    <div class="col-md-9 text-center zürich-west-text">
                        Hier vermischt sich international geprägte Urbanität
                        mit einem lebendigen, vielfarbigen Quartierleben.
                        Das einstige Industriegebiet zeigt eine pulsierende
                        Dynamik, dicht an dicht reihen sich Theater, Kinos,
                        Konzertsäle, Clubs, Bars, Cafés, Restaurants, Hotels,
                        Fitnesszentren und unzählige kleine Läden und Boutiquen.
                        Eine multikulturelle Ambiance, die sich laufend verändert
                        und neu inszeniert.
                    </div>
                </div>
            </div>
        </section>
        <sectino class="toni-areal">
            <div class="row">
                <div class="col-md-12 col-lg-7 col-xl-7">
                    <img src="/wp-content/uploads/2019/04/Prime_Tower_Toni_Areal_.jpg" alt="">
                </div>
                <div class="col-md-12 col-lg-5 col-xl-5">
                    <div class="toni-areal-text-cont">
                        <div class="toni-areal-title">Toni-Areal</div>
                        <div class="toni-areal-text">
                            Im neu aufgebauten Gebäudekomplex an der
                            Pfingstweidstrasse - dem ehemaligen Sitz der
                            Toni-Molkerei - befindet sich heute ein Hochschul-Campus,
                            in den im Herbst 2014 die Zürcher Hochschule der Künste (ZHdK)
                            und Teile der Zürcher Hochschule für Angewandte Wissenschaften
                            (ZHAW) einzogen.
                            <div class="toni-areal-arrow">
                                <a target="_blank" href="https://museum-gestaltung.ch/de/standort/toni-areal/"><img src="/wp-content/uploads/2019/04/Group461.png" alt=""></a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-lg-5 col-xl-5 order-lg-first order-last">
                    <div class="toni-areal-text-cont">
                        <div class="toni-areal-title">Schauspielhaus / Schiffbau</div>
                        <div class="toni-areal-text">
                            Seit dem Jahr 2000 ist der Schiffbau die zweite Spielstätte
                            des Zürcher Schauspielhauses, neben dem traditionellen Theater am Pfauen.
                            Der Schiffbau bietet in der Halle bis zu 600 Pätze und in der Box 200.
                            Dazu kommt die Matchbox mit 70 Plätzen für das Junge Schauspielhaus,
                            eine Bühne für Nachwuchstalente.
                            <div class="toni-areal-arrow">
                                <a target="_blank" href="http://schauspielhaus.ch/de/"><img src="/wp-content/uploads/2019/04/Group461.png" alt=""></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-lg-7 col-xl-7 order-lg-last order-first">
                    <img src="/wp-content/uploads/2019/04/Prime_Tower_Schiffbau_.jpg" alt="">
                </div>
            </div>
        </sectino>
    </main>
    <script src="<?=get_template_directory_uri();?>/assets/js/image-map.js"></script>
    <script>
        ImageMap('img[usemap]', 0);

        let dafaultBuilding = 'primeTower';

        function changeBuilding(building) {
            let selector = '.building-map';
            document.querySelector(selector).setAttribute('src', '/wp-content/themes/prime-tower/assets/images/areal/' + building + '.png');
            $('.building-content').addClass('d-none');
            $('.building-content.' + building).removeClass('d-none').addClass('d-blok');
        }

        function imgMapEnter(building) {
            changeBuilding(building);
        }

        function imgMapLeave() {
            changeBuilding(dafaultBuilding);
        }

        function imgMapClick(building) {
            changeBuilding(building);
            dafaultBuilding = building;
        }

        $( document ).ready(function() {
            $(".mieter-tabs-icon").on('click', function () {
                $( this ).parent().parent().toggleClass( 'show-mieter-tabs' );
            });
        });
    </script>


<?php get_footer();