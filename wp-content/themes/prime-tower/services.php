<?php
/*
Template Name: Services
Template Post Type: page
 */
get_header(); ?>

    <main class="services-page">
        <div class="full-thumbnail"
             style="background: url(<?=the_post_thumbnail_url('single-post-thumbnail');?>);">
        </div>
        <section class="kein-business">
            <div class="container">
                <div class="kein-business-sub-title">
                    Umfassend umsorgt
                </div>
                <div class="kein-business-title">
                    Für ein fokussiertes Arbeiten
                </div>
                <div class="kein-business-content">
                    Um im Prime Tower jederzeit eine entspannte und produktive Atmosphäre zu gewährleisten,
                    stehen den Nutzern eine Vielzahl professioneller Dienstleistungen zur Verfügung. So
                    erwarten Mieter und Gäste neben einer zentralen Rezeption im Erdgeschoss mit
                    biometrischer Zugangskontrolle zahlreiche weitere Services:
                </div>
            </div>
        </section>
        <section class="services">
            <div class="container">
                <div class="row justify-content-end">
                    <div class="col-md-12 mobile-services-padding">
                        <img style="width: 100%;" class="mieter-tabs-icon services-logo-img" src="/wp-content/uploads/2019/04/GettyImages-968845176.png">
                    </div>
                    <div class="col-12 service-container">
                        <img style="margin-bottom:45px" class="mieter-tabs-icon" src="/wp-content/uploads/2019/04/Bildschirmfoto 2019-02-14 um 09.19.53.png">
                        <div class="servise-title">Allcare Group</div>
                        <div class="servise-text">Die Walk-in-Arztpraxis im Cubus steht montags bis freitags von 7 bis 19 Uhr offen.</div>
                        <a href="https://allcare.ch/"><img class="mieter-tabs-icon" src="/wp-content/uploads/2019/04/Group461.png"></a>
                    </div>
                </div>
                <div class="row justify-content-start">
                    <div class="col-md-12 mobile-services-padding">
                        <img style="width: 100%;" class="mieter-tabs-icon services-logo-img" src="/wp-content/uploads/2019/05/Prime_Tower_Kieser.jpg">
                    </div>
                    <div class="col-12 service-container">
                        <img style="margin-bottom:45px" class="mieter-tabs-icon" src="/wp-content/uploads/2019/04/KieserTraining_logo.svg.png">
                        <div class="servise-title">Kieser Training</div>
                        <div class="servise-text">Hier sorgen kompetentes Coaching und professionelle Fitness-Geräte täglich für den
                            gesunden Körper zum gesunden Geist.</div>
                        <a href="https://www.kieser-training.ch/"><img class="mieter-tabs-icon" src="/wp-content/uploads/2019/04/Group461.png"></a>
                    </div>
                </div>
                <div class="row justify-content-end">
                    <div class="col-md-12 mobile-services-padding">
                        <img style="width: 100%;" class="mieter-tabs-icon services-logo-img" src="/wp-content/uploads/2019/04/Group626.png">
                    </div>
                    <div class="col-12 service-container">
                        <img style="margin-bottom:45px" class="mieter-tabs-icon" src="/wp-content/uploads/2019/04/Bildschirmfoto 2019-02-18 um 09.38.30.png">
                        <div class="servise-title">Physiokandil GmbH</div>
                        <div class="servise-text">Der Spezialist für Therapie und Training am Bewegungsapparat fördert hier die Gesundheit
                            durch aktive, passive und ergänzende Behandlungen.</div>
                        <a href="http://physiokandil.ch/"><img class="mieter-tabs-icon" src="/wp-content/uploads/2019/04/Group461.png"></a>
                    </div>
                </div>
                <div class="row justify-content-start">
                    <div class="col-md-12 mobile-services-padding">
                        <img style="width: 100%;" class="mieter-tabs-icon services-logo-img" src="/wp-content/uploads/2019/05/Prime_Tower_profawo.jpg">
                    </div>
                    <div class="col-12 service-container">
                        <img style="margin-bottom:45px" class="mieter-tabs-icon" src="/wp-content/uploads/2019/04/Bildschirmfoto 2019-02-14 um 09.21.03.png">
                        <div class="servise-title">Kids & Co</div>
                        <div class="servise-text">Die langjährige Erfahrung, gut aus- und weitergebildetes Personal und ein eigenes
                            pädagogisches Konzept bieten hier Kleinkindern äusserst professionelle Betreuungsplätze.</div>
                        <a href="https://www.profawo.ch/"><img class="mieter-tabs-icon" src="/wp-content/uploads/2019/04/Group461.png"></a>
                    </div>
                </div>
                <div class="row justify-content-end">
                    <div class="col-md-12 mobile-services-padding">
                        <img style="width: 100%;" class="mieter-tabs-icon services-logo-img" src="/wp-content/uploads/2019/04/Photo10.png">
                    </div>
                    <div class="col-12 service-container">
                        <img style="margin-bottom:45px" class="mieter-tabs-icon" src="/wp-content/uploads/2019/04/2000px-ZKB.svg.png">
                        <div class="servise-title">Zürcher Kantonalbank</div>
                        <div class="servise-text">Die ZKB-Filiale im Prime Tower empfängt Kundinnen und Kunden montags bis freitags von
                            9.30 bis 17 Uhr.</div>
                        <a href="https://www.zkb.ch/"><img class="mieter-tabs-icon" src="/wp-content/uploads/2019/04/Group461.png"></a>
                    </div>
                </div>
                <div class="row justify-content-start">
                    <div class="col-md-12 mobile-services-padding">
                        <img style="width: 100%;" class="mieter-tabs-icon services-logo-img" src="/wp-content/uploads/2019/04/Photo11.png">
                    </div>
                    <div class="col-12 service-container">
                        <img style="margin-bottom:45px" class="mieter-tabs-icon" src="/wp-content/uploads/2019/04/Coop.svg.png">
                        <div class="servise-title">Coop</div>
                        <div class="servise-text">Von Montag bis Samstag sorgt im Platform-Gebäude ein grosses Angebot an frischen
                            Produkten für den bequemen Einkauf.</div>
                        <a href="https://www.coop.ch"><img class="mieter-tabs-icon" src="/wp-content/uploads/2019/04/Group461.png"></a>
                    </div>
                </div>
                <div class="row justify-content-end">
                    <div class="col-md-12 mobile-services-padding">
                        <img style="width: 100%;" class="mieter-tabs-icon services-logo-img" src="/wp-content/uploads/2019/04/Photo12.png">
                    </div>
                    <div class="col-12 service-container">
                        <img style="margin-bottom:45px" class="mieter-tabs-icon" src="/wp-content/uploads/2019/04/Coop_Pronto_Logo.png">
                        <div class="servise-title">Coop Pronto</div>
                        <div class="servise-text">Täglich von früh morgens bis spät abends geöffnet für besonders schnelle und spontane
                            Wünsche.</div>
                        <a href="https://www.coop-pronto.ch"><img class="mieter-tabs-icon" src="/wp-content/uploads/2019/04/Group461.png"></a>
                    </div>
                </div>
            </div>
        </section>
    </main>
<?php get_footer();
