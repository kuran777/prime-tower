<?php
/*
Template Name: Contact
Template Post Type: page
 */
get_header(); ?>

    <main class="contact-page">
        <div class="full-thumbnail"
             style="background: url(<?=the_post_thumbnail_url('single-post-thumbnail');?>);">
        </div>
        <section class="contact-section">
            <div class="container">
                <div class="row justify-content-center">
<!--                    <div class="col-12 col-md-12 contact-page-title">Kontakt</div>-->
                    <div class="col-12 col-md-7 contact-text-title">Kompetente Betreuung Treten Sie mit uns in Kontakt</div>
                    <div class="col-12 col-md-12 contact-text-sub-title">Ich interessiere mich für</div>
                </div>
                <?php echo do_shortcode( '[contact-form-7 id="1234" title="Contact form 1"]' ); ?>
            </div>
        </section>
        <section class="bottom-contact">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-5">
                        <div class="bottom-contact-title">
                            PRIME TOWER EMPFANG
                        </div>
                        <div class="bottom-contact-text">
                            <p>Hardstrasse 201</p>
                            <p>8005 Zürich</p>
                            <p>Tel. +41 58 800 49 00</p>
                            <p>info@primetower.ch</p>
                        </div>
                        <div class="bottom-contact-soc">
                            <a href="mailto:info@primetower.ch"><img src="/wp-content/uploads/2019/04/message.png"></a>
                            <a href="tel:"><img src="/wp-content/uploads/2019/04/phone.png"></a>
                        </div>
                    </div>
                    <div class="col-12 col-md-5">
                        <div class="bottom-contact-title">
                            Areal Management
                        </div>
                        <div class="bottom-contact-text">
                            <p>Wincasa AG</p>
                            <p>Maschinenstrasse 11</p>
                            <p>Postfach</p>
                            <p>8005 Zürich</p>
                            <p>Telefon +41 44 403 28 30</p>
                            <p>nikolina.nikolendzic@wincasa.ch</p>
                        </div>
                        <div class="bottom-contact-soc">
                            <a href="mailto:info@primetower.ch"><img src="/wp-content/uploads/2019/04/message.png"></a>
                            <a href="tel:"><img src="/wp-content/uploads/2019/04/phone.png"></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
<?php get_footer();
