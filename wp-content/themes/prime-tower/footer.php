    <footer class="site-footer">
        <div class="container">
            <div class="row justify-content-center justify-content-md-start text-center text-md-left">
                <div class="col-md-12">
                    <img style="margin-bottom: 42px;" src="<?=get_template_directory_uri();?>/assets/images/footer-logo.svg" alt="">
                </div>
                <div class="col-md-4 col-lg-2">
                    <div style="margin-bottom: 30px">Kontakt/Empfang Prime Tower</div>
                    <div>Hardstrasse 201<br> 8005 Zürich<br><br> Telefon +41 58 800 49 00 info@primetower.ch</div>
                </div>
                <div class="col-md-4 col-lg-3">
                    <div style="margin-bottom: 10px">Areal Management</div>
                    <div>Wincasa AG<br> Maschinenstrasse 11<br> Postfach<br> 8005 Zürich><br><br>Telefon +41 44 403 28 30<br> nikolina.nikolendzic@wincasa.ch</div>
                </div>
                <div class="col-md-4 col-lg-2">
                    <div>Anfahrt</div>
                    <div><a style="color:#848484"  href="http://primetower.ch/webcam/livestream">Webcam</a> </div>
                    <div>Medien</div>
                    <div>Medienberichte</div>
				</div>
			<div class="col-md-4 col-lg-2">
                    <div style="margin-bottom: 20px">Medienstelle</div>
                    <div>Swiss Prime Site Immobilien AG <br> Frohburgstrasse 1<br> Postfach<br> CH-4601 Olten <br> <br> +41 58 317 17 17<br> mladen.tomic@sps.swiss <br> www.sps.swiss </div>
                </div>
                <div class="col-md-6 col-lg-3 futer-soc-cont text-center text-md-left">
                    <div style="margin-bottom: 20px">SOCIAL MEDIA</div>
                    <div class="socials-box justify-content-center justify-content-md-start ">
                        <div class="d-inline-flex">
                            <img class="mieter-tabs-icon" src="/wp-content/uploads/2019/04/soc1.png">
                        </div>
                        <div class="flex-grow-0 flex-md-grow-1">
                            <?= do_shortcode("[supsystic-social-sharing id='1']"); ?>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <?php
        wp_nav_menu(array(
            'container' => false,
            'menu_class' => '',
            'theme_location' => 'footer',
            'items_wrap' => '%3$s',
            'fallback_cb' => false
        ));
        ?>
    </footer>

</div>
</div>
</div>
</div>
</div>
<?php wp_footer(); ?>
</body>
</html>