<?php
add_theme_support('post-thumbnails');
add_image_size('single-post-thumbnail', 1440, 850);

function themename_custom_logo_setup()
{
    $defaults = array(
        'height' => 46,
        'width' => 166,
        'flex-height' => true,
        'flex-width' => true,
        'header-text' => array('site-title', 'site-description'),
    );
    add_theme_support('custom-logo', $defaults);
}

add_action('after_setup_theme', 'themename_custom_logo_setup');

show_admin_bar( false );