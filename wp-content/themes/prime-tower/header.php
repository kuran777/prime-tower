<?php
$page = get_queried_object();
?>
<!DOCTYPE html>
<!--[if lte IE 11]>
<html <?php language_attributes(); ?> class="no-js lte-ie11"> <![endif]-->
<!--[if gte IE 11]><!-->
<html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->
    <head>
        <meta charset="<?php bloginfo('charset'); ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="cleartype" content="on">
        <?php wp_head(); ?>
        <link rel='stylesheet' href='<?=get_template_directory_uri()?>/style.css' type='text/css' media='all'/>
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=latin-ext" rel="stylesheet">
        <?php
        $dev_hostname = "prime-tower.test";
        if ((strpos($_SERVER['SERVER_NAME'], ".") === false) || ($_SERVER['SERVER_NAME'] === $dev_hostname)) {
            echo "<script type=\"text/javascript\" id=\"__bs_script__\">document.write(\"<script async src='http://HOST:3000/browser-sync/browser-sync-client.js'><\/script>\".replace(\"HOST\", window.location.hostname));</script>";
        }
        ?>
    </head>
<body <?php body_class(); ?>>
<div id="st-container" class="st-container st-effect">
    <nav class="st-menu st-effect" id="st-menu">
        <div class="st-menu-btn"><i class="fa fa-times" aria-hidden="true"></i></div>
        <h2 class="icon icon-lab"><img src="/wp-content/uploads/2019/04/Group 502.png" alt=""></h2>
        <ul>
            <li><a class="icon icon-data" href="/business/">Business</a></li>
            <li><a class="icon icon-location" href="/servises/">Services</a></li>
            <li><a class="icon icon-study" href="/gastronomie/">Gastronomie</a></li>
            <li><a class="icon icon-photo" href="/areal/">Areal</a></li>
            <li><a class="icon icon-wallet" href="/contact/">Kontakt</a></li>
        </ul>
        <div class="socials-box-header justify-content-center">
            <div class="d-inline-flex">
                <img style="width: 25px; height: 25px; margin-top: 14px;" class="mieter-tabs-icon" src="/wp-content/uploads/2019/04/Path 459.png">
            </div>
            <div class="flex-grow-0">
                <?= do_shortcode("[supsystic-social-sharing id='1']"); ?>
            </div>
        </div>
    </nav>
    <div class="st-pusher">
    <div class="st-content"><!-- this is the wrapper for the content -->
    <div class="st-content-inner"><!-- extra div for emulating position:fixed of the menu -->
<div class="site-container">
    <header class="site-header">
        <div class="container">
            <div class="row justify-content-md-between justify-content-center">
                <div class="col-auto site-logo">
                    <?php
                    if ( function_exists( 'the_custom_logo' ) ) {
                        the_custom_logo();
                    }
                    ?>
                </div>
                <div class="col-auto">
                    <div style="transform: translateY(8px);" class="search d-inline-block">
                        <?php echo do_shortcode('[wpdreams_ajaxsearchlite]'); ?>
                    </div>
                    <div class="language d-inline-block">
                        <?=do_shortcode('[language-switcher]');?>
                    </div>
                    <div class="d-inline-block st-menu-btn">
                        <i style="cursor: pointer" class=""><img class="menu-icon" src="/wp-content/uploads/2019/04/Menu.png" alt=""></i>
                    </div>
                </div>
            </div>
        </div>


<!--        <a class="site-brand" href="--><?php //bloginfo('url'); ?><!--" title="--><?php //bloginfo('name'); ?><!--"-->
<!--           rel="home">--><?php //bloginfo('name'); ?><!--</a>-->
<!--        <a class="site-brand" href="--><?php //bloginfo('url'); ?><!--" title="--><?php //bloginfo('name'); ?><!--"-->
<!--           rel="home">--><?php //bloginfo('name'); ?><!--</a-->
<!--        --><?php //wp_nav_menu(array(
//            'theme_location' => 'primary',
//            'container' => false,
//            'items_wrap' => '%3$s', // removes the <ul> from the menu as we're using our own markup
//            'fallback_cb' => mytheme_menu_fallback
//        )); ?>

    </header>

<?php // do_shortcode('[instagram-feed user="vasiapolianchitch"]'); ?>