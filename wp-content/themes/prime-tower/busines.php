<?php
/*
Template Name: Business
Template Post Type: page
 */
get_header(); ?>

<main class="business-page">
    <div class="full-thumbnail"
         style="background: url(<?=the_post_thumbnail_url('single-post-thumbnail');?>),#F2F2F2; background-repeat:no-repeat;">
    </div>
    <section class="kein-business">
        <div class="container">
            <div class="kein-business-sub-title">
                Business District mit Zukunft
            </div>
            <div class="kein-business-title">
                In bester Gesellschaft
            </div>
            <div class="kein-business-content">
                Zahlreiche Unternehmen aus diversen Branchen haben das Prime Tower Areal zu ihrem
Firmenstandort erkoren. Es ist dank seiner modernen Gebäudeinfrastruktur, der
ausgezeichneten Erreichbarkeit und einer kreativen Umgebung mit vielseitigen
Nutzungsmöglichkeiten ein idealer Arbeitsort und Zürichs Business Hotspot schlechthin.
            </div>
        </div>
    </section>
    <section class="mieter">
            <div class="container">
            <div class="mieter-title">
                Mieterliste
            </div>
            <div class="meiter-content">
                <div class="mieter-tabs first">
                    <div class="mieter-tabs-title-cont">
                        <img data-togle="false" class="mieter-tabs-icon" src="/wp-content/uploads/2019/04/Group 1.png">
                        <span class="mieter-tabs-title">DIENSTLEISTUNG</span>
                    </div>
                    <div class="row mieter-tabs-shows">
                        <div class="col-12 col-md-8">
                            <span>Allcare Hausarzt-Zentren AG</span>
                        </div>
                        <div class="col-12 col-md-4 text-center text-md-right">
                            <a href="mailto:info@allcare.ch"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/message.png"></a>
                            <a href="tel:+41 43 311 20 22"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/phone.png"></a>
                            <a target="_blank" href="https://allcare.ch/"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/sup.png"></a>
                        </div>
                    </div>
                    <div class="row mieter-tabs-shows">
                        <div class="col-12 col-md-8">
                            <span>Galerie Eva Presenhuber AG</span>
                        </div>
                        <div class="col-12 col-md-4 text-center text-md-right">
                            <a href="mailto:info@presenhuber.com"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/message.png"></a>
                            <a href="tel:+41 43 444 70 50"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/phone.png"></a>
                            <a target="_blank" href="https://presenhuber.com/"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/sup.png"></a>
                        </div>
                    </div>
                    <div class="row mieter-tabs-shows">
                        <div class="col-12 col-md-8">
                            <span>Galerie Peter Kilchmann</span>
                        </div>
                        <div class="col-12 col-md-4 text-center text-md-right">
                            <a href="mailto:info@peterkilchmann.com"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/message.png"></a>
                            <a href="tel:+41 44 278 10 10"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/phone.png"></a>
                            <a target="_blank" href="https://peterkilchmann.com/"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/sup.png"></a>
                        </div>
                    </div>
                    <div class="row mieter-tabs-shows">
                        <div class="col-12 col-md-8">
                            <span>Kieser Training</span>
                        </div>
                        <div class="col-12 col-md-4 text-center text-md-right">
                            <a href="mailto:corporate@kieser-training.com"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/message.png"></a>
                            <a href="tel:+41 44 296 17 17"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/phone.png"></a>
                            <a target="_blank" href="https://kieser-training.com/"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/sup.png"></a>
                        </div>
                    </div>
                    <div class="row mieter-tabs-shows">
                        <div class="col-12 col-md-8">
                            <span>Kids & Co Prime Tower / profawo Zürich</span>
                        </div>
                        <div class="col-12 col-md-4 text-center text-md-right">
                            <a href="mailto:zuerich@profawo.ch"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/message.png"></a>
                            <a href="tel:+41 43 960 34 30"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/phone.png"></a>
                            <a target="_blank" href="https://profawo.ch/"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/sup.png"></a>
                        </div>
                    </div>
                    <div class="row mieter-tabs-shows">
                        <div class="col-12 col-md-8">
                            <span>MAAG Music & Arts AG</span>
                        </div>
                        <div class="col-12 col-md-4 text-center text-md-right">
                            <a href="mailto:office@mymaag.ch"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/message.png"></a>
                            <a href="tel:+41 44 444 26 26"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/phone.png"></a>
                            <a target="_blank" href="https://bymaag.ch/"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/sup.png"></a>
                        </div>
                    </div>
                    <div class="row mieter-tabs-shows">
                        <div class="col-12 col-md-8">
                            <span>Physio Kandil GmbH</span>
                        </div>
                        <div class="col-12 col-md-4 text-center text-md-right">
                            <a href="mailto:zh-prime@physiokandil.ch"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/message.png"></a>
                            <a href="tel:+41 44 212 38 00"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/phone.png"></a>
                            <a target="_blank" href="https://physiokandil.ch/"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/sup.png"></a>
                        </div>
                    </div>
                    <div class="row mieter-tabs-shows">
                        <div class="col-12 col-md-8">
                            <span>Tonhalle Gesellschaft Zürich</span>
                        </div>
                        <div class="col-12 col-md-4 text-center text-md-right">
                            <a href="mailto:info@tonhalle.ch"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/message.png"></a>
                            <a href="tel:+41 44 206 34 40"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/phone.png"></a>
                            <a target="_blank" href="https://tonhalle-orchester.ch/"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/sup.png"></a>
                        </div>
                    </div>
                    <div class="row mieter-tabs-shows">
                        <div class="col-12 col-md-8">
                            <span>Zürcher Kantonalbank</span>
                        </div>
                        <div class="col-12 col-md-4 text-center text-md-right">
                            <a href="tel:+41 844 843 823"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/phone.png"></a>
                            <a target="_blank" href="https://zkb.ch/"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/sup.png"></a>
                        </div>
                    </div>
                </div>
                <div class="mieter-tabs">
                    <div class="mieter-tabs-title-cont">
                        <img class="mieter-tabs-icon" src="/wp-content/uploads/2019/04/Group 1.png">
                        <span class="mieter-tabs-title">BÜRO</span>
                    </div>
                    <div class="row mieter-tabs-shows">
                        <div class="col-12 col-md-8">
                            <span>Assess + Perform AG</span>
                        </div>
                        <div class="col-12 col-md-4 text-center text-md-right">
                            <a href="mailto:info@assessandperform.ch"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/message.png"></a>
                            <a href="tel:+41 44 366 64 44"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/phone.png"></a>
                            <a target="_blank" href="https://assessandperform.ch/"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/sup.png"></a>
                        </div>
                    </div>
                    <div class="row mieter-tabs-shows">
                        <div class="col-12 col-md-8">
                            <span>Bouygues E&S FM Schweiz AG</span>
                        </div>
                        <div class="col-12 col-md-4 text-center text-md-right">
                           <a target="_blank" href="https://bouygues-es.ch/"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/sup.png"></a>
                        </div>
                    </div>
                    <div class="row mieter-tabs-shows">
                        <div class="col-12 col-md-8">
                            <span>Citibank (Switzerland) AG</span>
                        </div>
                        <div class="col-12 col-md-4 text-center text-md-right">
                            <a href="mailto:debra.ghittini@citi.com"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/message.png"></a>
                            <a href="tel:+41 58 750 50 00"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/phone.png"></a>
                            <a target="_blank" href="https://citigroup.com/"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/sup.png"></a>
                        </div>
                    </div>
                    <div class="row mieter-tabs-shows">
                        <div class="col-12 col-md-8">
                            <span>Deutsche Bank (Schweiz) AG</span>
                        </div>
                        <div class="col-12 col-md-4 text-center text-md-right">
                            <a href="mailto:kathrin.aeschlimann@db.com"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/message.png"></a>
                            <a href="tel:+41 58 111 01 11"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/phone.png"></a>
                            <a target="_blank" href="https://db.com/"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/sup.png"></a>
                        </div>
                    </div>
                    <div class="row mieter-tabs-shows">
                        <div class="col-12 col-md-8">
                            <span>Ernst & Young AG</span>
                        </div>
                        <div class="col-12 col-md-4 text-center text-md-right">
                            <a href="mailto:peter.fehlmann@ch.ey.com"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/message.png"></a>
                            <a href="tel:+41 58 286 31 11"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/phone.png"></a>
                            <a target="_blank" href="https://ey.com/"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/sup.png"></a>
                        </div>
                    </div>
                    <div class="row mieter-tabs-shows">
                        <div class="col-12 col-md-8">
                            <span>GAM</span>
                        </div>
                        <div class="col-12 col-md-4 text-center text-md-right">
                            <a href="mailto:info@gam.com"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/message.png"></a>
                            <a href="tel:+41 58 426 30 30"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/phone.png"></a>
                            <a target="_blank" href="https://gam.com/"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/sup.png"></a>
                        </div>
                    </div>
                    <div class="row mieter-tabs-shows">
                        <div class="col-12 col-md-8">
                            <span>Guido Schilling AG</span>
                        </div>
                        <div class="col-12 col-md-4 text-center text-md-right">
                            <a href="mailto:info@guidoschilling.ch"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/message.png"></a>
                            <a href="tel:+41 44 366 63 33"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/phone.png"></a>
                            <a target="_blank" href="https://guidoschilling.ch/"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/sup.png"></a>
                        </div>
                    </div>
                    <div class="row mieter-tabs-shows">
                        <div class="col-12 col-md-8">
                            <span>Homburger AG</span>
                        </div>
                        <div class="col-12 col-md-4 text-center text-md-right">
                            <a href="mailto:lawyers@homburger.ch"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/message.png"></a>
                            <a href="tel:+41 43 222 10 00"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/phone.png"></a>
                            <a target="_blank" href="https://homburger.ch/"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/sup.png"></a>
                        </div>
                    </div>
                    <div class="row mieter-tabs-shows">
                        <div class="col-12 col-md-8">
                            <span>Humanis AG</span>
                        </div>
                        <div class="col-12 col-md-4 text-center text-md-right">
                            <a href="mailto:info@humanis.ch"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/message.png"></a>
                            <a href="tel:+41 44 366 60 00"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/phone.png"></a>
                            <a target="_blank" href="https://www.humanis.ch/"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/sup.png"></a>
                        </div>
                    </div>
                    <div class="row mieter-tabs-shows">
                        <div class="col-12 col-md-8">
                            <span>Jones Lang LaSalle AG</span>
                        </div>
                        <div class="col-12 col-md-4 text-center text-md-right">
                            <a href="mailto:info.ch@eu.jll.com"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/message.png"></a>
                            <a href="tel:+41 44 215 75 00"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/phone.png"></a>
                            <a target="_blank" href="https://joneslanglasalle.ch/"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/sup.png"></a>
                        </div>
                    </div>
                    <div class="row mieter-tabs-shows">
                        <div class="col-12 col-md-8">
                            <span>Korn/Ferry International</span>
                        </div>
                        <div class="col-12 col-md-4 text-center text-md-right">
                            <a href="mailto:silvia.aznarmelgar@kornferry.com"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/message.png"></a>
                            <a href="tel:+41 43 366 77 88"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/phone.png"></a>
                            <a target="_blank" href="https://kornferry.com/"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/sup.png"></a>
                        </div>
                    </div>
                    <div class="row mieter-tabs-shows">
                        <div class="col-12 col-md-8">
                            <span>Netcentric AG</span>
                        </div>
                        <div class="col-12 col-md-4 text-center text-md-right">
                            <a href="mailto:info@netcentric.biz"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/message.png"></a>
                            <a href="tel:+41 43 508 20 28"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/phone.png"></a>
                            <a target="_blank" href="https://netcentric.biz/"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/sup.png"></a>
                        </div>
                    </div>
                    <div class="row mieter-tabs-shows">
                        <div class="col-12 col-md-8">
                            <span>Nexiot AG</span>
                        </div>
                        <div class="col-12 col-md-4 text-center text-md-right">
                            <a href="mailto:info@nexiot.ch"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/message.png"></a>
                            <a href="tel:+41 44 545 93 80"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/phone.png"></a>
                            <a target="_blank" href="https://nexiot.ch/"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/sup.png"></a>
                        </div>
                    </div>
                    <div class="row mieter-tabs-shows">
                        <div class="col-12 col-md-8">
                            <span>Oracle Software (Schweiz) GmbH</span>
                        </div>
                        <div class="col-12 col-md-4 text-center text-md-right">
                            <a href="tel:+41 56 483 31 11"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/phone.png"></a>
                            <a target="_blank" href="https://oracle.com/"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/sup.png"></a>
                        </div>
                    </div>
                    <div class="row mieter-tabs-shows">
                        <div class="col-12 col-md-8">
                            <span>Repower AG</span>
                        </div>
                        <div class="col-12 col-md-4 text-center text-md-right">
                            <a href="mailto:info@repower.com"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/message.png"></a>
                            <a href="tel:+41 81 839 70 00"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/phone.png"></a>
                            <a target="_blank" href="https://repower.com/"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/sup.png"></a>
                        </div>
                    </div>
                    <div class="row mieter-tabs-shows">
                        <div class="col-12 col-md-8">
                            <span>Schilling Partners AG</span>
                        </div>
                        <div class="col-12 col-md-4 text-center text-md-right">
                            <a href="mailto:info@schillingpartners.ch"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/message.png"></a>
                            <a href="tel:+41 44 366 63 00"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/phone.png"></a>
                            <a target="_blank" href="https://schillingpartners.ch/"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/sup.png"></a>
                        </div>
                    </div>
                    <div class="row mieter-tabs-shows">
                        <div class="col-12 col-md-8">
                            <span>Swiss Prime Site AG</span>
                        </div>
                        <div class="col-12 col-md-4 text-center text-md-right">
                            <a href="mailto:info@swiss-prime-site.ch"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/message.png"></a>
                            <a href="tel:+41 58 317 17 17"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/phone.png"></a>
                            <a target="_blank" href="https://swiss-prime-site.ch/"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/sup.png"></a>
                        </div>
                    </div>
                </div>
                <div class="mieter-tabs">
                    <div class="mieter-tabs-title-cont">
                        <img class="mieter-tabs-icon" src="/wp-content/uploads/2019/04/Group 1.png">
                        <span class="mieter-tabs-title">GASTRONOMIE</span>
                    </div>
                    <div class="row mieter-tabs-shows">
                        <div class="col-12 col-md-8">
                            <span>Bar Hotel Rivington & Sons</span>
                        </div>
                        <div class="col-12 col-md-4 text-center text-md-right">
                            <a href="mailto:contact@rivington.ch"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/message.png"></a>
                            <a href="tel:+41 43 366 90 82"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/phone.png"></a>
                            <a target="_blank" href="https://hotelrivingtonandsons.ch/"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/sup.png"></a>
                        </div>
                    </div>
                    <div class="row mieter-tabs-shows">
                        <div class="col-12 col-md-8">
                            <span>CLOUDS Restaurant und Conference</span>
                        </div>
                        <div class="col-12 col-md-4 text-center text-md-right">
                            <a href="mailto:info@clouds.ch"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/message.png"></a>
                            <a href="tel:+41 44 404 30 00"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/phone.png"></a>
                            <a target="_blank" href="https://clouds.ch/"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/sup.png"></a>
                        </div>
                    </div>
                    <div class="row mieter-tabs-shows">
                        <div class="col-12 col-md-8">
                            <span>EY Restaurant platform</span>
                        </div>
                        <div class="col-12 col-md-4 text-center text-md-right">
                            <a href="mailto:ey@zfv.ch"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/message.png"></a>
                            <a href="tel:+41 44 27359 22"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/phone.png"></a>
                            <a target="_blank" href="https://zfv.ch/de/microsites/ey-restaurant-platform/kontakt/"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/sup.png"></a>
                        </div>
                    </div>
                    <div class="row mieter-tabs-shows">
                        <div class="col-12 col-md-8">
                            <span>k2 Bar Bistro</span>
                        </div>
                        <div class="col-12 col-md-4 text-center text-md-right">
                            <a href="mailto:service@bymaag.ch"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/message.png"></a>
                            <a href="tel:+41 444 26 66"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/phone.png"></a>
                            <a target="_blank" href="https://k2bistro.ch/"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/sup.png"></a>
                        </div>
                    </div>
                    <div class="row mieter-tabs-shows">
                        <div class="col-12 col-md-8">
                            <span>LY’S ASIA</span>
                        </div>
                        <div class="col-12 col-md-4 text-center text-md-right">
                            <a href="mailto:contact@lys-asia.ch"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/message.png"></a>
                            <a href="tel:+41 44 999 08 08"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/phone.png"></a>
                            <a target="_blank" href="https://lys-asia.ch/"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/sup.png"></a>
                        </div>
                    </div>
                </div>
                <div class="mieter-tabs last">
                    <div class="mieter-tabs-title-cont">
                        <img class="mieter-tabs-icon" src="/wp-content/uploads/2019/04/Group 1.png">
                        <span class="mieter-tabs-title">RETAIL</span>
                    </div>
                    <div class="row mieter-tabs-shows">
                        <div class="col-12 col-md-8">
                            <span>Coop</span>
                        </div>
                        <div class="col-12 col-md-4 text-center text-md-right">
                            <a target="_blank" href="https://coop.ch/"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/sup.png"></a>
                        </div>
                    </div>
                    <div class="row mieter-tabs-shows">
                        <div class="col-12 col-md-8">
                            <span>Coop Pronto</span>
                        </div>
                        <div class="col-12 col-md-4 text-center text-md-right">
                            <a target="_blank" href="https://coop.ch/"><img class="mieter-tabs-soc-icon" src="/wp-content/uploads/2019/04/sup.png"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="hochkarätige">
        <div class="row">
            <div class="col-12 col-md-12 col-lg-6 no-padding">
                <img src="/wp-content/uploads/2019/04/Prime_Tower_Mietflaechen.jpg" alt="">
            </div>
            <div class="col-12 col-md-12 col-lg-6 text-center text-md-left no-padding">
                <div class="hochkarätige-content-cont">
                    <div class="hochkarätige-title">Hochwertige Mietflächen
Erfolg braucht Platz
</div>
                    <div class="hochkarätige-content">
                        Der Prime Tower und seine vier Nebengebäude bieten auf insgesamt mehr als 75’000 m2
skalierbare Büro-, Geschäfts-, Gastronomie- und Kulturflächen für höchste Ansprüche.
Lassen Sie sich hier inspirieren. Gerne beraten wir Sie persönlich über mögliche
Mietangebote auf dem Areal.

                    </div>
                    <div class="hochkarätige-btn">
                        <a class="eModal-3" href="">Freie Flächen</a>
                    </div>
                    <div class="hochkarätige-info">
                        <p>Roger Gut</p>
                        <p>Wincasa AG</p>
                        <p>Maschinenstrasse 11</p>
                        <p>8005 Zürich</p>
                        <p>Telefon +41 44 403 28 81</p>
                        <p>roger.gut@wincasa.ch</p>
                    </div>
                    <div class="hochkarätige-soc">
                         <a href="mailto:roger.gut@wincasa.ch"><img src="/wp-content/uploads/2019/04/message1.png"></a>
                        <a href="tel:+41444032881"><img src="/wp-content/uploads/2019/04/phone2.png"></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
    <script>
        $( document ).ready(function() {
            var plusSrc = "/wp-content/uploads/2019/04/Group 1.png";
            var minusSrc = "/wp-content/uploads/2019/04/Symbol108.png";
            $(".mieter-tabs-icon").on('click', function () {
                var toggle = $(this).data('togle');
                $( this ).parent().parent().toggleClass( 'show-mieter-tabs' );
                $(this).data('togle', !toggle);
                $(this).attr('src', !toggle ? minusSrc : plusSrc);
            });
        });
    </script>


<?php get_footer();
