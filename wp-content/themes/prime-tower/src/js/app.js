// Import Bootstrap
import 'bootstrap';
import AOS from 'aos';
import sass from '../scss/app.scss'


import 'aos/dist/aos.css';
import './modules/_header.js'
import './modules/_main.js'

AOS.init();
