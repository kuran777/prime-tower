$(function() {
    'use strict';
    console.log('main');

    $('.full-thumbnail').css({
        'background-attachment': 'fixed',
        'background-size': 'cover'
    });
    $(window).scroll(function () {
        $('.full-thumbnail').css('background-position', "0px " + (0 - (Math.max(document.documentElement.scrollTop, document.body.scrollTop) / 4)) + "px");
    });
});
