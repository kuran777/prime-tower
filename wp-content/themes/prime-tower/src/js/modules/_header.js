$(() => {
  'use strict';

  $('.st-menu-btn').click(() => {
    $(this).css('position', 'absolute');
    $('.st-container.st-effect').toggleClass('st-menu-open');
  })
});