<?php
/*
Template Name: Gastronomie
Template Post Type: page
 */
get_header(); ?>

    <main class="services-page">
        <div class="full-thumbnail"
             style="background: url(<?=the_post_thumbnail_url('single-post-thumbnail');?>);">
        </div>
        <section class="kein-business">
            <div class="container">
                <div class="kein-business-sub-title">
                    Gastronomie
                </div>
                <div class="kein-business-title">
                    Genuss à la carte
                </div>
                <div class="kein-business-content">
                    Auf dem Prime Tower Areal finden Sie Kulinarik nach Lust und Laune:
                    vom Gourmetrestaurant im 35. Stock über den Asiaten und den
                    Italiener bis zur stilvollen Coffee Bar oder Cocktail Lounge
                    werden unterschiedlichste Wünsche erfüllt. Für eilige Esser
                    wie für geruhsame Geniesser.
                </div>
            </div>
        </section>
        <section class="services">
            <div class="container">
                <div class="row justify-content-end">
                    <div class="col-md-12 mobile-services-padding">
                        <img style="width: 100%;" class="mieter-tabs-icon" src="/wp-content/uploads/2019/04/GettyImages.png">
                    </div>
                    <div class="col-12 service-container">
                        <img style="margin-bottom:45px" class="mieter-tabs-icon" src="/wp-content/uploads/2019/04/Group 423.png">
                        <div class="servise-title">Clouds</div>
                        <div class="servise-text">In 120 m Höhe ein exquisites Menu aus der Open Kitchen zelebrieren,
                            ist ein Erlebnis der besonderen Art.
                            Für private und exklusive Anlässe steht das Privé als separater Raum zur Verfügung.</div>
                        <a href="https://clouds.ch/"><img class="mieter-tabs-icon" src="/wp-content/uploads/2019/04/Group461.png"></a>
                    </div>
                </div>
                <div class="row justify-content-start">
                    <div class="col-md-12 mobile-services-padding">
                        <img style="width: 100%;" class="mieter-tabs-icon" src="/wp-content/uploads/2019/04/Group 558.png">
                    </div>
                    <div class="col-12 service-container">
                        <img style="margin-bottom:45px" class="mieter-tabs-icon" src="/wp-content/uploads/2019/04/Logo_Lys.png">
                        <div class="servise-title">Ly’s</div>
                        <div class="servise-text">Sei es zum leichten Lunch oder zum ausgiebigen Dinner - hier finden
                            Sie eine grosse Auswahl panasiatischer Gerichte im passenden Ambiente. Oder auch im Take-away.</div>
                        <a href="https://lys-asia.ch/"><img class="mieter-tabs-icon" src="/wp-content/uploads/2019/04/Group461.png"></a>
                    </div>
                </div>
                <div class="row justify-content-end">
                    <div class="col-md-12 mobile-services-padding">
                        <img style="width: 100%;" class="mieter-tabs-icon" src="/wp-content/uploads/2019/04/Group 620.png">
                    </div>
                    <div class="col-12 service-container">
                        <img style="margin-bottom:45px" class="mieter-tabs-icon" src="/wp-content/uploads/2019/04/Group 431.png">
                        <div class="servise-title">Rivington & Sons</div>
                        <div class="servise-text">Ein Donut zum Espresso, ein Bier zum Lunch und Stevie Wonder zur
                            Happy Hour - die Bar Hotel Rivington & Sons im Erdgeschoss des Prime Tower begleitet Sie im
                            Stil des Big Apple durch den Tag.</div>
                        <a href="https://hotelrivingtonandsons.ch/"><img class="mieter-tabs-icon" src="/wp-content/uploads/2019/04/Group461.png"></a>
                    </div>
                </div>
                <div class="row justify-content-start">
                    <div class="col-md-12 mobile-services-padding">
                        <img style="width: 100%;" class="mieter-tabs-icon" src="/wp-content/uploads/2019/04/Group 621.png">
                    </div>
                    <div class="col-12 service-container">
                        <img style="margin-bottom:45px" class="mieter-tabs-icon" src="/wp-content/uploads/2019/04/logo_2.png">
                        <div class="servise-title">K2 Bar Bistro</div>
                        <div class="servise-text">Einfache gute Küche, vernünftige Preise und ein angesagtes
                            Kulturambiente - das sind die Zutaten, die den Besuch im Bistro k2 in der MAAG Halle,
                            unmittelbar neben dem Prime Tower, zum Erlebnis machen.</div>
                        <a href="https://k2bistro.ch/"></a><img class="mieter-tabs-icon" src="/wp-content/uploads/2019/04/Group461.png"></a>
                    </div>
                </div>
                <div class="row justify-content-end">
                    <div class="col-md-12 mobile-services-padding">
                        <img style="width: 100%;" class="mieter-tabs-icon" src="/wp-content/uploads/2019/04/c_744_10_gastraum.png">
                    </div>
                    <div class="col-12 service-container">
                        <img style="margin-bottom:45px" class="mieter-tabs-icon" src="/wp-content/uploads/2019/04/profile_image_32816.png">
                        <div class="servise-title">EY Restaurant platform</div>
                        <div class="servise-text">On weekdays the EY staff cafeteria in the Platform Building
                            also welcomes external visitors in for a quick bite at a reasonable price.</div>
                        <a href="https://zfv.ch/de/microsites/ey-restaurant-platform"><img class="mieter-tabs-icon" src="/wp-content/uploads/2019/04/Group461.png"></a>
                    </div>
                </div>
            </div>
        </section>
    </main>
<?php get_footer();
